module mux#
(
    parameter DATA_WIDTH = 32,
)
(
    input logic [DATA_WIDTH-1:0] data0, data1, data2, data3,
    input logic [1:0] select,
    output logic [DATA_WIDTH-1:0] out
);

    always_comb
    begin
        case (select)
            2'b00: out = data0[DATA_WIDTH-1:0];
            2'b01: out = data1[DATA_WIDTH-1:0];
            2'b10: out = data2[DATA_WIDTH-1:0];
            2'b11: out = data3[DATA_WIDTH-1:0];
        endcase
    end // always_comb

    endmodule // mux#