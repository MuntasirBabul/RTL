// FIFO Implement

module fifo(
    input logic clk, rst_n, wr, rd, 
    input logic [7:0] data_in, 
    output logic [7:0] data_out, 
    output logic fifo_full, fifo_empty, fifo_threshold, fifo_overflow, fifo_underflow
);

logic [4:0] wptr, rptr;
logic fifo_we, fifo_rd; 

write_pointer u_write_pointer(
    .clk            (clk        ), 
    .rst_n          (rst_n      ), 
    .wr             (wr         ), 
    .fifo_full      (fifo_full  ),
    .fifo_we        (fifo_we    ),
    .wptr           (wptr       ));

read_pointer u_read_pointer(
    .clk            (clk        ), 
    .rst_n          (rst_n      ), 
    .rd             (rd         ),
    .fifo_empty     (fifo_empty ),
    .fifo_rd        (fifo_rd    ),
    .rptr           (rptr       ));

status_signal u_status_signal(
    .clk            (clk        ), 
    .rst_n          (rst_n      ), 
    .wr             (wr         ), 
    .rd             (rd         ), 
    .fifo_we        (fifo_we    ), 
    .fifo_rd        (fifo_rd    ),
    .wptr           (wptr       ), 
    .rptr           (rptr       ),
    .fifo_full      (fifo_full  ), 
    .fifo_empty     (fifo_empty ), 
    .fifo_threshold (fifo_threshold), 
    .fifo_overflow  (fifo_overflow), 
    .fifo_underflow (fifo_underflow));

memory_array u_memory_array(
    .clk            (clk        ), 
    .fifo_we        (fifo_we    ),
    .data_in        (data_in    ),
    .wptr           (wptr       ), 
    .rptr           (rptr       ),
    .data_out       (data_out   ));
endmodule:fifo
    
module memory_array(
    input logic clk, fifo_we,
    input logic [7:0] data_in,
    input logic [4:0] wptr, rptr,
    output logic [7:0] data_out
);

logic [7:0] data_out_temp [15:0];

always@(posedge clk)
begin 
    if(fifo_we)
    data_out_temp[wptr[3:0]] <= data_in;
end
assign data_out = data_out_temp[rptr[3:0]];
endmodule:memory_array

module write_pointer(
    input logic clk, rst_n, wr, fifo_full,
    output logic [4:0] wptr,
    output logic fifo_we
);

assign fifo_we = ~fifo_full & wr;

always@(posedge clk or negedge rst_n)
begin 
    if(~rst_n)
        wptr <= '0;
    else if(fifo_we)
        wptr <= wptr + 5'b00001;
    else    
        wptr <= wptr; // make sure this works
end
endmodule:write_pointer

module read_pointer(
    input logic clk, rst_n, rd, fifo_empty,
    output logic fifo_rd,
    output logic [4:0] rptr
);

assign fifo_rd = rd & ~fifo_empty;
always@(posedge clk or negedge rst_n)
begin 
    if(~rst_n)
        rptr <= '0;
    else if(fifo_rd)
        rptr <= rptr + 5'b00001;
    else    
        rptr <= rptr; // make sure this works 
end
endmodule:read_pointer

module status_signal(
    input logic clk, rst_n, wr, rd, fifo_we, fifo_rd,
    input logic [4:0] wptr, rptr,
    output logic fifo_full, fifo_empty, fifo_threshold, fifo_overflow, fifo_underflow
);

logic overflow_set, underflow_set, fbit_comp, pointer_equal;
logic [4:0] pointer_result;

assign fbit_comp = wptr[4] ^ rptr[4];
assign pointer_equal = (wptr[3:0] - rptr[3:0]) ? 0:1;
assign pointer_result = wptr[4:0] - rptr[4:0];
assign overflow_set = fifo_full & wr;
assign underflow_set = fifo_empty & rd;

always@(*)
begin 
    fifo_full = fbit_comp & pointer_equal;
    fifo_empty = ~fbit_comp & pointer_equal;
    fifo_threshold = pointer_result[4] | pointer_result[3] ? 1:0;
end

always@(posedge clk or negedge rst_n)
begin 
    if(~rst_n)
        fifo_underflow <= 0;
    else if(underflow_set==1 && fifo_we ==0)
        fifo_underflow <= 1;
    else if(fifo_we)
        fifo_underflow <= 0;
    else    
        fifo_underflow <= fifo_underflow; // make sure this works
end

always@(posedge clk or negedge rst_n)
begin 
    if(~rst_n)
        fifo_overflow <= 0;
    else if(overflow_set == 1 && fifo_we == 0)
        fifo_overflow <= 1;
    else if(fifo_rd)
        fifo_overflow <= 0;
    else fifo_overflow <= fifo_overflow;

end
endmodule:status_signal

