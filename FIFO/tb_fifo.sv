`timescale 1ns/1ns

module tb_top();

// Parameters ////////////////////////////////////////////////////

parameter CYCLE     = 10;
parameter END_TIME  = 500;

// Signals ///////////////////////////////////////////////////////

logic clk; 
logic rst_n;
logic wr;
logic rd;
logic [7:0] data_in;
logic [7:0] data_out;
logic fifo_empty;
logic fifo_full;
logic fifo_threshold;
logic fifo_overflow;
logic fifo_underflow;

int i;

// DUT ///////////////////////////////////////////////////////////

fifo u_fifo(
    .clk            (clk), 
    .rst_n          (rst_n), 
    .wr             (wr),     
    .rd             (rd), 
    .data_in        (data_in), 
    .data_out       (data_out), 
    .fifo_full      (fifo_full), 
    .fifo_empty     (fifo_empty), 
    .fifo_threshold (fifo_threshold), 
    .fifo_overflow  (fifo_overflow), 
    .fifo_underflow (fifo_underflow));

// Methods ///////////////////////////////////////////////////////

task apply_reset();
begin 
    #(CYCLE) rst_n = 1'b1;
    #(CYCLE) rst_n = 1'b0;
    #(CYCLE) rst_n = 1'b1;
end
endtask:apply_reset

task clock_gen();
begin 
    forever #(CYCLE/2) clk = ~clk; 
end
endtask:clock_gen

task opr_process();
begin 
    for(i=0;i<20;i++)
    begin: WRE    
        #(5)
        wr = 1'b1;
        data_in = data_in + 8'b1;
        #(5)
        wr = 1'b0;
    end
    for(i=0;i<20;i++)
    begin: RDE 
        #(2)
        rd = '1;
        #(2)
        rd = '0;
    end
end
endtask:opr_process

task debug_fifo();
begin 
    $display("------------------------------------------------------------");
    $display("-------------------- SIMULATION RESULT ---------------------");
    $display("------------------------------------------------------------");
    $display("\n");
    $display("TIME = %d, wr = %b, rd = %b, data_in = %h", $time, wr, rd, data_in);
end
endtask:debug_fifo

task end_sim();
begin 
    #(END_TIME);
    $display("------------------ FINISHED SIMULATION ---------------------");
    $finish;
end
endtask

// Main ///////////////////////////////////////////////////////////

initial begin 
    clk     = 1'b0;
    rst_n   = 1'b0;
    wr      = 1'b0;
    rd      = 1'b0;
    data_in = 8'b0;
end
 
initial begin
    fork 
    apply_reset();
    clock_gen();
    opr_process();
    debug_fifo();
    end_sim();
    join
end

// self-checking ////////////////////////////////////////////////

logic [5:0] waddr, raddr;
logic [7:0] mem[64:0];

always@(posedge clk) 
begin 
    if(~rst_n) 
        waddr = '0;
    else if(wr)
    begin 
        mem[waddr] <= data_in;
        waddr++;
    end
    $display("TIME = %d, data_out = %d, meme = %d", $time, data_out, mem[raddr]);
    if(~rst_n)
        raddr <= '0;
    else if(rd & ~fifo_empty)
        raddr <= raddr++;
        if(mem[raddr]==data_out) begin 
            $display("=== PASS ===== PASS ==== PASS ==== PASS ===");  
            if (raddr == 16) $finish;
        end
        else begin  
                $display ("=== FAIL ==== FAIL ==== FAIL ==== FAIL ===");  
                $display("-------------- THE SIMUALTION FINISHED ------------");  
                $finish;
        end 
end

initial begin 
    dumpvars;
    dumpfile("dump.vcd");
end
endmodule