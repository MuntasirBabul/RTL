import cocotb
from cocotb.triggers import Timer, RisingEdge, FallingEdge, ClockCycles
import random
import os
import pytest
from cocotb.clock import Clock
from cocotb.regression import TestFactory
from cocotb.triggers import RisingEdge, FallingEdge, Join

# Read NUM_TESTS
if ("NUM_TESTS" in os.environ):
    NUM_TESTS = int(os.environ["NUM_TESTS"])
else:
    NUM_TESTS = 1

# Main Test
async def test_reg(dut, NUM_TESTS):
    # Generating and Running clock
    clock = Clock(dut.clk, 2, units="ns")
    cocotb.fork(clock.start())
    await RisingEdge(dut.clk)
    
    dut.rstn.value <= 1
    await RisingEdge(dut.clk)
    dut.rstn.value <= 0
    await RisingEdge(dut.clk)
    dut.rstn.value <= 1

    dut_bin_res = []
    # Initializing Inputs
    wdata = random.randint(0,65535)
    addr  = random.randint(0,3)
    await RisingEdge(dut.clk)

    driver_task = cocotb.fork(driver(dut, wdata, addr ))
    monitor_task = cocotb.fork(monitor(dut, addr))
    await Join(driver_task)
    dut_bin_res = await(monitor_task)
    
    await RisingEdge(dut.clk)
    print(dut_bin_res[0])
    print((wdata))
    assert dut_bin_res[0] == str(wdata) , "Error in Register"
    
# Driver
async def driver(dut, wdata, addr):
  dut.wr.value      <= 1
  dut.sel.value     <= 1
  dut.wdata.value   <= wdata
  dut.addr.value    <= addr
  await RisingEdge(dut.clk)
  await RisingEdge(dut.clk)
  
# Monitor
async def monitor(dut, addr):
  await RisingEdge(dut.clk)
  await RisingEdge(dut.clk)
  bin_result = []
  dut.addr.value = addr
  dut.sel.value = 1
  dut.wr.value = 0
  res_str = dut.rdata.value.binstr
  bin_result.append(str(res_str))
  return bin_result

# Generate Tests
tf = TestFactory(test_function=test_reg)
tf.add_option('NUM_TESTS', [x for x in range(NUM_TESTS)])
tf.generate_tests()