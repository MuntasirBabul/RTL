module des 
(
  input logic clk, rstn, wr, sel,
  input logic [15:0] wdata,
  input logic [1:0] addr,
  output logic [15:0] rdata
);

logic [15:0] register [0:3];
integer i;

always@(posedge clk) begin 
  if(!rstn) begin 
    for (i = 0;i < 4; i = i+1) begin 
      register[i] <= 0;
    end
  end
  else begin 
    if(sel & wr)
      register[addr] <= wdata;
    else
      register[addr] <= register[addr];
  end
end

assign rdata = (sel & ~wr) ? register[addr] : 0;

endmodule