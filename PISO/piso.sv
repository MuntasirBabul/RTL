module parallel_in_serial_out_shift_register#
(   
    parameter DATA_WIDTH = 512
)
(
    input logic [511:0] data_in,
    input logic clk,
    input logic rst_n,
    output logic serial_out
);

logic [511:0] shift_register;

always_ff @(posedge clk or negedge rst_n) begin
    if (!rst_n) begin
        shift_register <= 0;
    end else begin
        shift_register <= {shift_register[510:0], data_in[511]};
    end
end

assign serial_out = shift_register[511];

endmodule